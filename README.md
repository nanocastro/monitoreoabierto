# Monitoreo abierto de la calidad del aire

## Objetivo general
El proyecto tiene como objetivo desarrollar y recopilar recursos y herramientas abiertas para el monitoreo para cambiar la forma en que la calidad del aire se construye y presenta hoy en la esfera pública.

![Mapa cognitivo calidad del aire](/mapacognitivoaire.png )

### Nuestra inspiración 
https://www.youtube.com/watch?v=ybtrMw8wgw0

### Documentación de prototipos desarrollados
Actualmente estamos utilizando el prototipo de [Sensor Community](https://sensor.community/es/).  
Este proyecto es una continuación del desarrollo de prototipos abiertos para el monitoreo de la calidad del aire [MACA](https://gitlab.com/nanocastro/Repo_maca/).  

## Monitoreos

### Mapa con datos de sensores de partículas 
[Mapa Sensor Community](https://maps.sensor.community/#11/-32.889/-68.840)

### Mediciones de calidad del aire durante la cuarentena
Analisis de los datos de equipos de referencia de la UMM operada por el Lab. de Análisis Instrumental (Facultad Ingeniería - UNCuyo)  
[Resultados preliminares](https://rpubs.com/nanocastro/aq_cuarentena)

### Evaluación sensores low-cost 
Analisis de datos obtenidos durante la co-locación de un equipo de referencia Thermo junto a 4 replicas de los dispositivos Luftdaten para la medición de material particulado entre el 6 de abril y el 7 de mayo    
[Resultados preliminares](https://rpubs.com/nanocastro/605722)  
Se observa una correlación aceptable de los sensores low-cost respecto a la medición de PM10 de la referencia (R2>0.6 aprox)

### Seis meses de datos en la ciudad
[Aqui](https://docs.google.com/document/d/136wVyzHu81YjCtG7-f8o-LtgxURwIzd3h0CrBS6hgtw/edit) pueden ver las graficas de 6 meses de datos de PM10 y PM2.5 obtenidos con un sensor PMS7003 ubicado en un balcon aproximadamente a 15 metros del nivel de la intersección de las calles España y Sarmiento de la ciudad de Mendoza.

## Review de dispositivos low-cost a nivel local y global
Esta revisión podria avanzar hacia un review paper  
https://gitlab.com/gosh-community/air-quality-review  

## Recopilación de recursos abiertos y publicaciones 
https://forum.openhardware.science/t/air-quality-resources-on-open-source-low-cost-monitoring/1527?u=nanocastro
https://www.zotero.org/groups/360158/monitor_abierto/items/collectionKey/DCP4ZDSN

### Herramientas para el análisis de datos 
Paquete OpenAir para Luftdaten.info  
http://api.rpubs.com/nanocastro/  
https://nanocastro.shinyapps.io/OpenairLuftdaten/

## Workshops
https://docs.google.com/document/d/1uWtKyES0HKMkVZ_QlF3UPBoRF3uwraE-jbly-6VGzAo/edit

## Cómo contribuir al proyecto?  
Escribir a ferhcastro at gmail.com

## Documentación asociada
Proyecto Secyt
https://drive.google.com/open?id=1v4yF1FjXhbw4Z9PV1PNr4OnO9tlq5QWy
